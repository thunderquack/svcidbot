﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;
using Telegram.Bot;
using System.Net.Http;
using AzureInOut;
using System.Text.RegularExpressions;
using System.Threading;

namespace SvcIdBot
{
    internal class SvcIdBot : TelegramBotClient
    {
        private System.Timers.Timer Timer = new System.Timers.Timer(900000);
        private AzureIO IO;

        public SvcIdBot(string Token, string AzureConnectionString, string ShareName) : base(Token)
        {
            //Id = 98071669
            OnMessage += SvcIdBot_OnMessage;
            OnInlineQuery += SvcIdBot_OnInlineQuery;

            IO = new AzureIO(AzureConnectionString, ShareName);
            Timer.Elapsed += Timer_Elapsed;
            Timer.AutoReset = false;
            Timer.Start();
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Timer.Stop();
            try
            {
                Console.WriteLine("15 minutes passed, performing the web request");
                var hostName = string.Format("https://{0}.azurewebsites.net", Environment.ExpandEnvironmentVariables("%WEBSITE_SITE_NAME%")).ToLower();
                Console.WriteLine("Address: " + hostName);
                HttpClient httpClient = new HttpClient();
                var result = httpClient.GetAsync(hostName).Result;
            }
            catch (Exception err)
            {
                Console.WriteLine("Error: " + err.Message);
            }
            Timer.Start();
        }

        private void SvcIdBot_OnInlineQuery(object sender, Telegram.Bot.Args.InlineQueryEventArgs e)
        {
        }

        private void SvcIdBot_OnMessage(object sender, Telegram.Bot.Args.MessageEventArgs e)
        {
            try
            {
                //var c = GetChatAsync(-1001228946795).Result;
                if (e.Message.ForwardFrom != null)
                {
                    SendTextMessageAsync(e.Message.Chat.Id, e.Message.ForwardFrom.Id.ToString());
                    //IO.AppendAllText("Log.txt", e.Message.Chat.Id.ToString() + ";" + e.Message.From.Id.ToString() + ";" + e.Message.ForwardFrom.Id.ToString());
                }
                else
                {
                    if (e.Message.ForwardFromChat != null)
                    {
                        SendTextMessageAsync(e.Message.Chat.Id, e.Message.ForwardFromChat.Id.ToString());
                        //IO.AppendAllText("Log.txt", e.Message.Chat.Id.ToString() + ";" + e.Message.From.Id.ToString() + ";" + e.Message.ForwardFrom.Id.ToString());
                    }
                    else
                    {
                        string query = e.Message.Text;
                        if (Regex.Match(query, "^\\d+$").Success)
                        {
                            string Response = String.Format("<a href=\"tg://user?id={1}\">{0}</a>", "user" + query, query);
                            SendTextMessageAsync(e.Message.Chat.Id, Response, Telegram.Bot.Types.Enums.ParseMode.Html);
                        }
                        else
                        {
                            if (!e.Message.Text.StartsWith("@"))
                            {
                                query = "@" + query;
                            }
                            SendTextMessageAsync(e.Message.Chat.Id, GetChatAsync(new Telegram.Bot.Types.ChatId(query)).Result.Id.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SendTextMessageAsync(e.Message.Chat.Id, ex.Message);
            }
            if (e.Message.Text?.ToLower() == "error")
            {
                Console.WriteLine("Error command");
                //throw new Exception("error");
            }
        }
    }
}