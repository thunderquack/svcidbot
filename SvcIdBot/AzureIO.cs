﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.File;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;

namespace AzureInOut
{
    class AzureIO : IDisposable
    {
        private CloudStorageAccount storageAccount;
        private CloudFileClient fileClient;
        private CloudFileShare share;
        private CloudFileDirectory rootDir;
        private string Share;
        private CredentialCache Credentials;

        public AzureIO(string ConnectionString, string ShareName)
        {
            string StorageConnectionString = ConnectionString;
            storageAccount = CloudStorageAccount.Parse(StorageConnectionString);
            fileClient = storageAccount.CreateCloudFileClient();
            share = fileClient.GetShareReference(ShareName);
            rootDir = share.GetRootDirectoryReference();
            Credentials = new CredentialCache();
            var m=Regex.Match(ConnectionString, "(?i)AccountKey=(.*?);");
            string pwd = m.Groups[1].Value.ToString();
            Share = @"\\" + storageAccount.FileStorageUri.PrimaryUri.Host + @"\" + share.Name;
            NetworkCredential credential = new NetworkCredential(storageAccount.Credentials.AccountName, pwd);
            Credentials.Add(new Uri(Share), "Basic", credential);
        }
        public void AppendAllLines(string FileName, IEnumerable<string> value)
        {
            List<string> strings = new List<string>(value);
            if (strings.Count > 0)
            {
                CloudFile sourceFile = rootDir.GetFileReference(FileName);
                if (sourceFile.ExistsAsync().Result)
                {
                    sourceFile.DownloadToFileAsync(FileName, FileMode.Create).Wait();                    
                }
                File.AppendAllLines(FileName, value);
                sourceFile.UploadFromFileAsync(FileName).Wait();
                File.Delete(FileName);
            }
        }
        public void AppendAllText(string FileName, string value)
        {
            string f = Path.Combine(Share, FileName);
            File.AppendAllText(f, value);
            if (value.Length > 0)
            {
                CloudFile sourceFile = rootDir.GetFileReference(FileName);
                if (sourceFile.ExistsAsync().Result)
                {
                    sourceFile.DownloadToFileAsync(FileName, FileMode.Create).Wait();
                }
                File.AppendAllText(FileName, value);
                sourceFile.UploadFromFileAsync(FileName).Wait();
                File.Delete(FileName);
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~AzureIO() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
